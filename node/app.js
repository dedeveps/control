﻿var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.get('/api/pais', async function (request, response) {
	var paises = require('./paises.json');
    response.json(paises);
});

const path = require('path');
app.use(express.static(path.join(__dirname, 'public')));
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

var server = app.listen(2024, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("SUN running on %s:%s", host, port)
});
