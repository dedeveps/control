import logo from './usac.png';
import './App.css';
import useSWR from 'swr';

const fetcher = (...args) => fetch(...args).then((res) => res.json());

const prod = {
 API_URL: 'https://desarrollo.virtual.usac.edu.gt/epscontrol/'
};

const dev = {
 API_URL: 'http://localhost:2024'
};

const config = process.env.NODE_ENV === 'development' ? dev : prod;

function App() {

  const {
    data: paises,
    error,
    isValidating,
  } = useSWR(config.API_URL+'/api/pais', fetcher);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Versión de código 5.
        </p>
	    <div className="banderas">
		  <p>
            Fetch /api/pais
          </p>
          {paises &&
          paises.map((country, index) => (
            <img key={index} src={country.flags.png} alt='flag' width={100} />
          ))}
	    </div>
      </header>
    </div>
  );
}

export default App;
