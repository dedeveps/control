<b>Proyecto de ejemplo de integraci�n continua</b><br/>
<br/>
Carpetas:<br/>
<br/>
<b>node</b><br/>
<br/>
Contiene la aplicaci�n backend. Para correr de forma local se ejecutan los siguientes comandos:<br/>
<br/>
cd node<br/>
npm install <br/>
node app.js<br/>
<br/>
<b>react</b><br/>
<br/>
Contiene la aplicaci�n frontened. Para correr de forma local de ejecutan los siguientes comandos:<br/>
<br/>
cd react<br/>
npm install<br/>
npm run start<br/>
<br/>
<b>servicios</b><br/>
<br/>
Contiene el docker-compose para correr la aplicaci�n utilizando Docker. Es necesario informar que el docker-compose establece la carpeta de volumenes bind para permitir que la carpeta de build sea la carpeta public de node, de esta forma, al momento de generar los archivos de produccion de react, node podr� acceder a estos desde public.<br/>
<br/>
Es posible utilizar este servicio directamente, sin necesidad de instalar node y react en el sistema operativo host.<br/>
<br/>
El servicio crea una imagen de MySQL, la contrase�a del usuario root y eps est�n en el docker-compose.<br/>
<br/>
Instrucciones para utilizar la carpeta de servicios:<br/>
<br/>
1. docker-compose up<br/>
2. enjoy!<br/>
<!-- haha -->
<br/>